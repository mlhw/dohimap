# README #

DohiMaps  
v 1.0

### Explanation ###

The solutions is an iOS app where the user can view trails on a map. Tap on trails to see the trail name and tap on the info button to see more info about the trail.

To view POI Places togheter with the trail POI places must be turn on in settings

Filter trails by turning them on or off in settings

### Details ###

* iOS app written in Swift
* Deployment Target: 10.2


### Installation instructions ###

* Open project in Xcode
* Run in Simulator or device
