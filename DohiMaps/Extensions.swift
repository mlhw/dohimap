//
//  Extensions.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-08.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit
import MapKit

extension MKMapView {
    /// Helper method to zoom in to a point on the map
    /// - paramters: 
    ///     - point: The point where to zoom in to
    ///     - span: The span that should be visible from the point
    ///     - animated: True if zooming in should be animated
    func gotoPlace(_ point: CLLocationCoordinate2D, span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01), animated:Bool = true) {
        UIView.animate(withDuration: 1.5, animations: {
            let region1 = MKCoordinateRegion(center: point, span: span)
            self.setRegion(region1, animated: animated)
        })
    }
}


extension UIImageView {
    /// Helper method to  asynchronously download an image from a URL and set it to the image view
    ///     - url: The URL to the image
    ///     - mode: The Content mode to set to the image view for the linked image
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    /// Helper method to set an image from a link to a UIImageView
    /// - parameters:
    ///     - link: The link or path to the image
    ///     - mode: The Content mode to set to the image view for the linked image
    func downloadedFrom(link: String?, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let link = link, let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
