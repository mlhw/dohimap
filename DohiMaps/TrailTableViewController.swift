//
//  TrailTableViewController.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-07.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit

/// Shows trail info
class TrailTableViewController: UITableViewController {

    /// Image view for the trail path image
    @IBOutlet weak var headerImage: UIImageView!
    /// Label to show info about the trail path
    @IBOutlet weak var pathInfoLabel: UILabel!
    
    /// The HikingTrail to view
    var hikingTrail:HikingTrail!
    /// List of POI places for the hiking trail
    var places = [Place]()
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.headerImage.downloadedFrom(link: hikingTrail.imagePath)
        self.pathInfoLabel.text = hikingTrail.pathInfo
        self.places = hikingTrail.places
        self.title = hikingTrail.title
        
        // TODO: Show place_media items for this place
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell", for: indexPath)
        
        // Configure the cell...
        let place = self.places[indexPath.row];
        cell.textLabel?.text = place.title;

        return cell
    }
    



    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let cell = sender as? UITableViewCell, let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        if let destination = segue.destination as? PlaceViewController {
            let place = self.places[indexPath.row]
            destination.place = place
        }
    }


}

