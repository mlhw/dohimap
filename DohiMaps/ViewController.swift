//
//  ViewController.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-07.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit
import MapKit

/// Root view controller shows the map with trails and points of interest
class ViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    /// List of loaded hiking trails
    var trails = [HikingTrail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Start loading data from server
        loadData();
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Update the map if there has been any changes in what map objects to show or hide
        self.updateMap()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /// Fetch trail data from the server
    func loadData() {
        HikingTrail.fetchTrails { (trails, error) in
            // Handle error
            if let error = error {
                NSError.showError(error: error)
                return
            }
            
            self.trails = trails;
            
            DispatchQueue.main.async(execute: {
                self.updateMap()
                
                // TODO: Use point from trails instead of hardcoded point
                let point = CLLocationCoordinate2DMake(63.825537805635527, 20.265387296676636)
                self.mapView.gotoPlace(point)
            })
        }
    }
    
    /// Reload the maps annotations and overlays
    func updateMap() {
        // Remove all annotations
        let annotations = self.mapView.annotations
        self.mapView.removeAnnotations(annotations)
        
        // Remove all overlays
        let overlays = self.mapView.overlays
        self.mapView.removeOverlays(overlays)
        
        // Add annotations and overlays for the trails and POI that should be visible
        for trail in trails {
            if trail.showTrail {
                if let polyline = trail.polyline {
                    self.mapView.add(polyline)
                    self.mapView.addAnnotation(polyline)
                }
                
                if trail.showPOI {
                    let places = trail.places
                    self.mapView.addAnnotations(places)
                }
            }
        }
        
    }
    
    //MARK: Map Delegate Methods
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Place {
            let identifier = "pin"
            let view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            
            return view
        } else {
            let identifier = "other"
            let view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView{
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
                view.pinTintColor = UIColor.green
            }
            return view
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.red
            polylineRenderer.lineWidth = 2
            return polylineRenderer
        } else {
            let polylineRenderer = MKOverlayRenderer(overlay: overlay)
            return polylineRenderer
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let annotation = view.annotation as? Trail {
            self.performSegue(withIdentifier: "showTrail", sender: annotation.data)
        } else if let annotation = view.annotation as? Place {
            self.performSegue(withIdentifier: "showPlace", sender: annotation)
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let hikingTrail = sender as? HikingTrail, let destination = segue.destination as? TrailTableViewController {
            destination.hikingTrail = hikingTrail
        } else if let place = sender as? Place, let destination = segue.destination as? PlaceViewController {
            destination.place = place
        } else if let destination = segue.destination as? SettingsTableViewController {
            destination.trails = self.trails
        }
    }

    // MARK: Helper methods
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // Allow simultanius recognitions for tapping polyline should not interfere with thapping on annotations
        return true;
    }
    
    @IBAction func didTapMap(_ sender: UITapGestureRecognizer) {
        // Check if a trails polyline was tapped
        if let polyline = self.isTappedOnPolyline(with: sender, on: self.mapView) {
            print("Tapped: \(polyline.title)")
            self.mapView.selectAnnotation(polyline, animated: true);
        }
        
        
    }
    
    /// Check if polyline overlay was tapped
    func isTappedOnPolyline(with tapGesture:UITapGestureRecognizer, on mapView: MKMapView) -> MKPolyline? {
        let tappedMapView = tapGesture.view
        let tappedPoint = tapGesture.location(in: tappedMapView)
        let tappedCoordinates = mapView.convert(tappedPoint, toCoordinateFrom: tappedMapView)
        let point:MKMapPoint = MKMapPointForCoordinate(tappedCoordinates)
        
        let overlays = mapView.overlays.filter { o in
            o is MKPolyline
        }
        
        for overlay in overlays {
            let polygonRenderer = MKPolylineRenderer(overlay: overlay)
            let datPoint = polygonRenderer.point(for: point)
            polygonRenderer.invalidatePath()
            
            if (polygonRenderer.path.contains(datPoint)) {
                return overlay as? MKPolyline
            }
        }
        return nil
    }
    

    // MARK: Debug Helper methods
    
    // Helper method to zoom in to a predefinded place
    @IBAction func showPlace(_ sender: UIButton) {
        let point:CLLocationCoordinate2D
        if (sender.tag == 0) {
            // Umeå
            point = CLLocationCoordinate2DMake(63.825537805635527, 20.265387296676636)
        } else if (sender.tag == 1) {
            // Aus
            point = CLLocationCoordinate2DMake(-25.197912831386418,133.20150375366211)
        } else if (sender.tag == 2){
            // Skell
            point = CLLocationCoordinate2DMake(64.720079209609111,21.116065979003906)
            self.mapView.gotoPlace(point, span: MKCoordinateSpanMake(0.25, 0.25))
            return
        }
        else {
            point = self.mapView.centerCoordinate
            self.mapView.gotoPlace(point, span: MKCoordinateSpanMake(5.25, 5.25))
        }
        
        self.mapView.gotoPlace(point)
    }
    
    
}




