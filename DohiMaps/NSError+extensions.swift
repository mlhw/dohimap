//
//  NSError+extensions.swift
//  Wishes
//
//  Created by Mattias Lundhaw on 2015-12-12.
//  Copyright © 2015 ByBella. All rights reserved.
//

import UIKit

extension NSError {
    
    /// Convenience method to create a NSError from a JSONError
    ///
    /// - parameters:
    ///     - fault: The JSON error to create NSError from
    convenience init?(fault:JSONError?) {
        guard let fault = fault else {
            return nil
        }
        
        let domain = "DohiMaps"
        let code:Int = fault.errorCode()
        var userInfo:[AnyHashable : Any] = [:]
        
        userInfo[NSLocalizedFailureReasonErrorKey] = fault.rawValue
        
        //  NSError(domain: domain, code: code, userInfo: userInfo)
        self.init(domain: domain, code: code, userInfo: userInfo)
    }
    
    /// Display an error in an Alert dialog
    class func showError(error:Error) {
        if let fault = error as? JSONError {
            let nsError = NSError(fault: fault)
            nsError?.showError()
        } else {
            let nsError = error as NSError
            nsError.showError()
        }
    }
    
    /// Show the error with an alert dialog
    ///
    /// - parameters:
    ///     - title:    The title for the dialog. Defaults to Error
    ///     - message:  Optional error message. Defaults to self localizedDescription
    ///     - log:      Boolean for if the error should be logged or not
    func showError(title:String = "Error", message:String? = nil, log:Bool = true) {
        if log {
            self.logError()
        }
        
        let errorMessage:String
        if let m = message {
            errorMessage = m
        } else {
            errorMessage = self.localizedDescription
        }
        let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        let root = UIApplication.shared.delegate?.window??.rootViewController
        if let rootNav = root as? UINavigationController {
            NSError.showError(alert, fromViewController: rootNav.topViewController)
        } else {
            NSError.showError(alert, fromViewController: root)
        }
    }
    
    /// Show the error alert from a view controller
    ///
    /// - parameters:
    ///     - alert: The alert to show
    ///     - viewController: The viewController from where to show the alert dialog
    class func showError(_ alert:UIAlertController, fromViewController viewController:UIViewController?) {
        if let vc = viewController?.presentedViewController {
            vc.present(alert, animated: true, completion: nil)
        } else {
            viewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func logError() {
        print(self)
    }
}
