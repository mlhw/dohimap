//
//  SettingsTableViewCell.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-07.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var showTrailSwitch:UISwitch!
    @IBOutlet weak var showPOISwitch:UISwitch!
    
    var hikingTrail:HikingTrail?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateValues() {
        guard let trail = self.hikingTrail else { return }
        
        self.titleLabel.text = trail.title
        self.showPOISwitch.isOn = trail.showPOI
        self.showTrailSwitch.isOn = trail.showTrail
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func toggleTrail(_ sender:UISwitch) {
        guard let trail = self.hikingTrail else { return }
        
        trail.showTrail = sender.isOn
    }
    
    @IBAction func togglePOI(_ sender:UISwitch) {
        guard let trail = self.hikingTrail else { return }
        
        trail.showPOI = sender.isOn
    }
}
