//
//  PlaceViewController.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-07.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit
import MapKit


/// Shows info about a POI place
class PlaceViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!

    /// The POI place to show in this view
    var place:Place!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup place info
        self.title = self.place.title
        self.infoLabel.text = self.place.place_info
        self.imageView.downloadedFrom(link: self.place.place_image)
        
        // Show this place on the map
        self.mapView.addAnnotation(self.place)
        self.mapView.gotoPlace(self.place.coordinate, animated: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: Map Delegate Methods
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Place {
            let identifier = "pin"
            let view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = false
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            
            return view
        }
        
        return nil
    }

}
