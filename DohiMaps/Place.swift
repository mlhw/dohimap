//
//  Place.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-07.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit
import MapKit

/// Custom MKAnnotation class to show a trails POI place
class Place: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    let place_image: String
    let place_radius: Int
    var subtitle: String?
    var place_info: String?
    
    /// A week reference to the Hiking Trail object where this is a POI place
    weak var trail:HikingTrail!
    
    init?(data:[String:Any], _ trail:HikingTrail?) {
        guard
            let name = data["place_name"] as? String,
            let image = data["place_image"] as? String,
            let radius = data["place_radius"] as? Int,
            let pos = data["place_position"] as? [String:CLLocationDegrees],
            let lat = pos["lat"],
            let lng = pos["lng"]
        else {
            // Return nil when not all of above data exist in correct format
            return nil
        }
        
        
        if name.isEmpty {
            self.title = "Unknown"
        } else {
            self.title = name
        }
        
        self.subtitle = trail?.title
        self.trail = trail
        
        self.place_image = image
        self.place_radius = radius
        self.place_info = data["place_info"] as? String
    
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng);
        self.coordinate = coordinate
        
        super.init()
    }
}
