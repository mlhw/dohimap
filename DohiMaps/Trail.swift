//
//  Trail.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-08.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit
import MapKit

/// A Custom MKPolyline that holds a reference to the HikingTrail it represents
class Trail: MKPolyline {
    /// A week reference to the Hiking Trail object that this trail is for
    weak var data:HikingTrail!
    
}

extension MKPolyline {
    /// Convenience mehtod to create an instance of a MKPolyline with an array of CLLocationCoordinate2D coordinates
    /// - parameters:
    ///     - coords: The array of CLLocationCoordinate2D coordinates to create the line from
    convenience init(coordinates coords: Array<CLLocationCoordinate2D>) {
        let unsafeCoordinates = UnsafeMutablePointer<CLLocationCoordinate2D>.allocate(capacity: coords.count)
        unsafeCoordinates.initialize(from: coords)
        self.init(coordinates: unsafeCoordinates, count: coords.count)
        unsafeCoordinates.deallocate(capacity: coords.count)
    }
}
