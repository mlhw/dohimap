//
//  HikingTrail.swift
//  DohiMaps
//
//  Created by Mattias Lundhaw on 2017-03-07.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit
import MapKit


///  The Hiking Trail data class.
///  Holds all the info about a hiking trail.
class HikingTrail: NSObject {
    /// True if this trails POI should be visible on the map
    var showPOI:Bool = false
    /// True if this trail should be visible on the map
    var showTrail:Bool = true;
    
    /// The JSON data from the server
    let data:[String:Any]
    
    /// The title of path name for this trail
    var title:String? {
        return self.data["path_name"] as? String
    }
    
    /// A list of POI places for this
    var places = [Place]()
    
    /// Get the polyline for this trail
    var polyline:MKPolyline? {
        // Check that the path_polyline object has excpeted data array
        guard let pathJSON = data["path_polyline"] as? [[[String:CLLocationDegrees]]] else {
            return nil
        }
        
        var points = [CLLocationCoordinate2D]()
        for posArray in pathJSON {
            for pos in posArray {
            
                guard let lat = pos["lat"], let lng = pos["lng"] else {
                    continue
                }
            
                let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng);
                points.append(coordinate)
            }
        }
        
        // Create a Polyline Trail from the points and pass
        let trail = Trail(coordinates: points)
        trail.data = self
        trail.title = self.data["path_name"] as? String ?? "Unknown";
        return trail
    }
    
    /// URL path for the image at path_image
    var imagePath:String? {
        return data["path_image"] as? String
    }
    
    /// The path_info for this trail
    var pathInfo:String? {
        return data["path_info"] as? String
    }
    
    /// Instantiate a HikingObject from the JSON data input
    ///
    /// Returns nil if the input values are invalid
    /// - parameters: 
    ///     - json: The JSON data input
    init?(json:[String:Any]) {
        // TODO: Validate input
        guard let name = json["path_name"] as? String, !name.isEmpty else {
            return nil
        }
        
        self.data = json
        
        super.init()
        
        self.initPlaces()
    }
    
    /// Helper method to create Place MKAnnotations from the places JSON data
    private func initPlaces() {
        var places = [Place]()
        if let placesJSON = data["places"] as? [[String:Any]] {
            for item in placesJSON {
                
                if let place = Place(data: item, self) {
                    places.append(place)
                }
            }
        }
        self.places = places
    }
   
    
    

    /// Fetch hiking trails from the server
    /// - parameters:
    ///     - language: The language for the hiking trails. Defaults to sv
    ///     - completion: A completion block to be run after fetching data has been completed
    ///         - trails: a list of trails
    ///         - error: The error if any
    static func fetchTrails(language:String = "sv", completion: (([HikingTrail], _ error: Error?) -> Void)?) {
        let urlPath = "https://forward-byte-711.appspot.com/read/Test/Development/\(language)"
        guard let url = URL(string: urlPath) else {
            completion?([], JSONError.InvalidURL)
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            var trails = [HikingTrail]()
            do {
                guard let data = data else {
                    throw JSONError.NoData
                }
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: [ .allowFragments, .mutableContainers]) as? [String:Any] else  {
                    throw JSONError.InvalidJSON
                }
                
                // Convert to Hiking Trail Objects
                guard let paths = json["paths"] as? [[String:Any]] else {
                    throw JSONError.InvalidJSON
                }
                
                
                for pathJSON in paths {
                    if let trail = HikingTrail(json: pathJSON){
                        trails.append(trail)
                    } else {
                        // TODO: Allow invalid trail objects to pass unnoticed
                        // or throw invalid JSON exception
                    }
                    
                }
                
                completion?(trails, error);
                
                
            } catch let jsonError as JSONError {
                print(jsonError.rawValue)
                completion?(trails, error)
            } catch let nsError as NSError {
                completion?(trails, nsError)
            }
        }.resume()
    }
}

/// Custom Error object to handle JSON errors
enum JSONError: String, Error {
    case NoData = "ERROR: No data"
    case InvalidJSON = "ERROR: Invalid JSON."
    case InvalidURL = "ERROR: Invalid URL"
   
    func errorCode() -> Int {
        switch self {
        case .NoData:
            return 1
        case .InvalidJSON:
            return 2
        case .InvalidURL:
            return 3
        }
    }
}

